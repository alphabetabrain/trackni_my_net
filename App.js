import React from 'react';
import { StyleSheet } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import HomeScreen from './components/HomeScreen';
import PlotScreen from './components/PlotScreen';
import ModelScreen from './components/ModelScreen';

import Home from './components/images/home.svg';
import HomeActive from './components/images/home_active.svg';
import Plot from './components/images/plot.svg';
import PlotActive from './components/images/plot_active.svg';
import Model from './components/images/model.svg';
import ModelActive from './components/images/model_active.svg';

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
      <NavigationContainer>
        <Tab.Navigator
            barStyle={styles.bar}
        >
          <Tab.Screen
            name='Home'
            component={HomeScreen}
            options={{
              tabBarIcon: ({focused}) => {return focused ? <HomeActive /> : <Home />}
            }}
          />
          <Tab.Screen
            name='Plots'
            component={PlotScreen}
            options={{
              tabBarIcon: ({focused}) => {return focused ? <PlotActive /> : <Plot />}
            }}
          />
          <Tab.Screen
            name='Model'
            component={ModelScreen}
            options={{
              tabBarIcon: ({focused}) => {return focused ? <ModelActive /> : <Model />}
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  bar: {
    backgroundColor: 'white'
  }
})
