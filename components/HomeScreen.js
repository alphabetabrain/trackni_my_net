import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, TouchableOpacity } from 'react-native';
import { Overlay } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import Logo from './images/brain.svg';
import * as FileSystem from 'expo-file-system';


const saveFileUri = FileSystem.cacheDirectory + `tmn.save`;


export default class HomeScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '6218Q',
            overlay: false
        };

        this.onTokenChange = this.onTokenChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.toggleOverlay = this.toggleOverlay.bind(this);
    }

    async componentDidMount() {
        let res = await FileSystem.readAsStringAsync(saveFileUri)
        if (res) {
            this.setState({
                token: res,
                overlay: this.state.overlay
            });
        }
    }

    onTokenChange(token) {
        this.setState({
            token: token,
            overlay: this.state.overlay
        });
    }

    async onSubmit() {
        global.token = this.state.token;
        await FileSystem.writeAsStringAsync(saveFileUri,this.state.token)
        this.props.navigation.navigate('Plots')
    }

    toggleOverlay() {
        this.setState({
            overlay: !this.state.overlay
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <LinearGradient
                    style={styles.header}
                    colors={['#683BEB', '#0D0938']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                >
                    <Text style={styles.caption}>TRACKNI_MY_NET</Text>
                    <Logo style={styles.logo} />
                </LinearGradient>
                <Text style={styles.text}>Enter the key:</Text>
                <TextInput
                    style={styles.input}
                    placeholder='Token'
                    onChangeText={this.onTokenChange}
                    value={this.state.token}
                />
                <TouchableOpacity
                    style={[styles.button, styles.submit]}
                    onPress={this.onSubmit}
                >
                    <Text style={styles.button_title}>Submit</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={[styles.button, styles.about]}
                    onPress={this.toggleOverlay}
                >
                    <Text style={styles.button_title}>About</Text>
                </TouchableOpacity>
                <Overlay
                    isVisible={this.state.overlay}
                    onBackdropPress={this.toggleOverlay}
                >
                    <View style={styles.overlay}>
                        <Text style={styles.tagline}>
                            Monitor the training of your machine learning model anytime, anywhere with TRACKNI_MY_NET!
                        </Text>
                        <Text style={styles.tagline}>
                            Watch the status of your training with plot screen and model graph
                        </Text>
                        <View style={styles.developers}>
                            <Text style={styles.developer}>Developers</Text>
                            <Text style={styles.developer}>Gleb Bayer</Text>
                            <Text style={styles.developer}>Mikhail Sokolov</Text>
                            <Text style={styles.developer}>Roman Lyskov</Text>
                        </View>
                    </View>
                </Overlay>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center'
    },
    header: {
        width: '100%',
        height: 300,
        display: 'flex',
        alignItems: 'center',

        backgroundColor: '#683BEB',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40
    },
    caption: {
        marginTop: 55,
        marginBottom: 20,

        fontSize: 36,
        lineHeight: 54,
        textAlign: 'center',
        color: '#58FFFF'
    },
    text: {
        marginTop: 80,

        fontSize: 18,
        lineHeight: 27,
        textAlign: 'center'
    },
    input: {
        width: 370,
        height: 45,
        marginTop: 5,
        paddingLeft: 20,
        paddingRight: 20,

        fontSize: 24,
        borderColor: '#C4C4C4',
        borderWidth: 1,
        borderRadius: 30
    },
    button: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 120,
        height: 45,

        backgroundColor: '#2E1B7A',
        borderRadius: 30
    },
    button_title: {
        fontSize: 18,
        lineHeight: 27,
        color: 'white'
    },
    submit: {
        marginTop: 20
    },
    about: {
        marginTop: 60
    },
    overlay: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 300,
        height: 500,
    },
    tagline: {
        marginTop: 70,
        marginLeft: 40,
        marginRight: 40,

        fontSize: 18,
        lineHeight: 27,
        textAlign: 'center',
        color: '#2E187A'
    },
    developers: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 70,
        marginBottom: 30
    },
    developer: {
        fontSize: 14,
        lineHeight: 21,
        color: `rgba(196, 196, 196, 0.97)`
    }
})
