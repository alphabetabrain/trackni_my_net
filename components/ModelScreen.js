import React, { Component } from 'react';
import { View, StyleSheet, Text, Alert, ScrollView, RefreshControl, Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { SvgXml } from 'react-native-svg';

export default class PlotScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            response: null
        };

        this.onRefresh = this.onRefresh.bind(this);

        this.chartConfig = {
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0,
            color: opacity => '#683BEB'
        };
    }

    componentDidMount() {
        this.onRefresh();
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.onRefresh();
        })
    }

    componentWillUnmount() {
        this.props.navigation.removeListener('focus');
    }

    onRefresh() {
        this.setState({
            refreshing: true,
            response: null
        });
        fetch(`https://tracknimynet.herokuapp.com/?token=${global.token}`)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    refreshing: false,
                    response: json,
                });
            })
            .catch(error => {});
    }

    render() {
        let xml = '';
        if (this.state.response !== null)
            xml = this.state.response.model.replace('fill="#ffffff"', '');

        return (
            <ScrollView
                contentContainerStyle={styles.container}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >
                <LinearGradient
                    style={styles.header}
                    colors={['#683BEB', '#0D0938']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                >
                    <Text style={styles.caption}>Model</Text>
                </LinearGradient>

                {this.state.response !== null
                ? <View style={styles.model}>
                    <SvgXml xml={xml} />
                </View>
                : null}

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center'
    },
    header: {
        width: '100%',
        height: 128,
        display: 'flex',
        alignItems: 'center',

        backgroundColor: '#683BEB',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40
    },
    caption: {
        marginTop: 55,
        marginBottom: 20,

        fontSize: 24,
        lineHeight: 36,
        textAlign: 'center',
        color: '#58FFFF'
    },
    model: {
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        padding: 10,

        borderColor: '#683BEB',
        borderWidth: 3,
        borderRadius: 30
    }
})
