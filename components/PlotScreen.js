import React, { Component } from 'react';
import { View, StyleSheet, Text, ScrollView, RefreshControl, Alert } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { LineChart } from 'react-native-chart-kit';

export default class PlotScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            response: null
        };

        this.onRefresh = this.onRefresh.bind(this);

        this.chartConfig = {
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0,
            color: opacity => '#683BEB'
        };
    }

    componentDidMount() {
        this.onRefresh();
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.onRefresh();
        })
    }

    componentWillUnmount() {
        this.props.navigation.removeListener('focus');
    }

    onRefresh() {
        this.setState({
            refreshing: true,
            response: null
        });
        fetch(`https://tracknimynet.herokuapp.com/?token=${global.token}`)
            .then(response => response.json())
            .then(json => {
                this.setState({
                    refreshing: false,
                    response: json,
                });
            })
            .catch(error => {});
    }

    render() {
        const charts = [];

        if (this.state.response !== null && this.state.response.metrics.length > 1) {
            const metrics = {};

            const metric_names = Object.keys(this.state.response.metrics[0]);
            for (const metric_name of metric_names) {
                const metric = this.state.response.metrics.map(metric => metric[metric_name]);
                metrics[metric_name] = metric;
            }

            for (const metric_name in metrics) {
                if (metric_name === 'epoch')
                    continue;
                charts.push(
                    <View
                        style={styles.chart}
                        key={metric_name}
                    >
                        <LineChart
                            data={{
                                labels: metrics.epoch,
                                datasets: [{
                                    data: metrics[metric_name],
                                    color: opacity => '#683BEB'
                                }]
                            }}
                            width={380}
                            height={220}
                            chartConfig={this.chartConfig}
                        />
                        <Text style={styles.title}>{metric_name}</Text>
                    </View>
                );
            }
        }

        return (
            <ScrollView
                contentContainerStyle={styles.container}
                refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >
                <LinearGradient
                    style={styles.header}
                    colors={['#683BEB', '#0D0938']}
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                >
                    <Text style={styles.caption}>Plots</Text>
                </LinearGradient>
                {charts}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        alignItems: 'center'
    },
    header: {
        width: '100%',
        height: 128,
        display: 'flex',
        alignItems: 'center',

        backgroundColor: '#683BEB',
        borderBottomLeftRadius: 40,
        borderBottomRightRadius: 40
    },
    caption: {
        marginTop: 55,
        marginBottom: 20,

        fontSize: 24,
        lineHeight: 36,
        textAlign: 'center',
        color: '#58FFFF'
    },
    chart: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 10,
        width: 400,
        height: 300,

        borderColor: '#683BEB',
        borderWidth: 3,
        borderRadius: 30,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 18,
        color: '#683BEB'
    }
})
